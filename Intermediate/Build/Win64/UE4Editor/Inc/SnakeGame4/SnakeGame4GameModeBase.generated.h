// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEGAME4_SnakeGame4GameModeBase_generated_h
#error "SnakeGame4GameModeBase.generated.h already included, missing '#pragma once' in SnakeGame4GameModeBase.h"
#endif
#define SNAKEGAME4_SnakeGame4GameModeBase_generated_h

#define SnakeGame4_Source_SnakeGame4_SnakeGame4GameModeBase_h_15_RPC_WRAPPERS
#define SnakeGame4_Source_SnakeGame4_SnakeGame4GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define SnakeGame4_Source_SnakeGame4_SnakeGame4GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASnakeGame4GameModeBase(); \
	friend SNAKEGAME4_API class UClass* Z_Construct_UClass_ASnakeGame4GameModeBase(); \
public: \
	DECLARE_CLASS(ASnakeGame4GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/SnakeGame4"), NO_API) \
	DECLARE_SERIALIZER(ASnakeGame4GameModeBase) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define SnakeGame4_Source_SnakeGame4_SnakeGame4GameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesASnakeGame4GameModeBase(); \
	friend SNAKEGAME4_API class UClass* Z_Construct_UClass_ASnakeGame4GameModeBase(); \
public: \
	DECLARE_CLASS(ASnakeGame4GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/SnakeGame4"), NO_API) \
	DECLARE_SERIALIZER(ASnakeGame4GameModeBase) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define SnakeGame4_Source_SnakeGame4_SnakeGame4GameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnakeGame4GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnakeGame4GameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeGame4GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeGame4GameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeGame4GameModeBase(ASnakeGame4GameModeBase&&); \
	NO_API ASnakeGame4GameModeBase(const ASnakeGame4GameModeBase&); \
public:


#define SnakeGame4_Source_SnakeGame4_SnakeGame4GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnakeGame4GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeGame4GameModeBase(ASnakeGame4GameModeBase&&); \
	NO_API ASnakeGame4GameModeBase(const ASnakeGame4GameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeGame4GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeGame4GameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnakeGame4GameModeBase)


#define SnakeGame4_Source_SnakeGame4_SnakeGame4GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define SnakeGame4_Source_SnakeGame4_SnakeGame4GameModeBase_h_12_PROLOG
#define SnakeGame4_Source_SnakeGame4_SnakeGame4GameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame4_Source_SnakeGame4_SnakeGame4GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	SnakeGame4_Source_SnakeGame4_SnakeGame4GameModeBase_h_15_RPC_WRAPPERS \
	SnakeGame4_Source_SnakeGame4_SnakeGame4GameModeBase_h_15_INCLASS \
	SnakeGame4_Source_SnakeGame4_SnakeGame4GameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeGame4_Source_SnakeGame4_SnakeGame4GameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame4_Source_SnakeGame4_SnakeGame4GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	SnakeGame4_Source_SnakeGame4_SnakeGame4GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	SnakeGame4_Source_SnakeGame4_SnakeGame4GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	SnakeGame4_Source_SnakeGame4_SnakeGame4GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SnakeGame4_Source_SnakeGame4_SnakeGame4GameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
