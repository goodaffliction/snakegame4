// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "PlayerPawnBase.h"
PRAGMA_DISABLE_OPTIMIZATION
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePlayerPawnBase() {}
// Cross Module References
	SNAKEGAME4_API UClass* Z_Construct_UClass_APlayerPawnBase_NoRegister();
	SNAKEGAME4_API UClass* Z_Construct_UClass_APlayerPawnBase();
	ENGINE_API UClass* Z_Construct_UClass_APawn();
	UPackage* Z_Construct_UPackage__Script_SnakeGame4();
	SNAKEGAME4_API UClass* Z_Construct_UClass_ASnakeBase_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	ENGINE_API UClass* Z_Construct_UClass_UCameraComponent_NoRegister();
// End Cross Module References
	void APlayerPawnBase::StaticRegisterNativesAPlayerPawnBase()
	{
	}
	UClass* Z_Construct_UClass_APlayerPawnBase_NoRegister()
	{
		return APlayerPawnBase::StaticClass();
	}
	UClass* Z_Construct_UClass_APlayerPawnBase()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_APawn();
			Z_Construct_UPackage__Script_SnakeGame4();
			OuterClass = APlayerPawnBase::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= (EClassFlags)0x20900080u;


				UProperty* NewProp_SnakeActorClass = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("SnakeActorClass"), RF_Public|RF_Transient|RF_MarkAsNative) UClassProperty(CPP_PROPERTY_BASE(SnakeActorClass, APlayerPawnBase), 0x0014000000010001, Z_Construct_UClass_ASnakeBase_NoRegister(), Z_Construct_UClass_UClass());
				UProperty* NewProp_SnakeActor = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("SnakeActor"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(SnakeActor, APlayerPawnBase), 0x0010000000000004, Z_Construct_UClass_ASnakeBase_NoRegister());
				UProperty* NewProp_PawnComponent = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("PawnComponent"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(PawnComponent, APlayerPawnBase), 0x001000000008000c, Z_Construct_UClass_UCameraComponent_NoRegister());
				static TCppClassTypeInfo<TCppClassTypeTraits<APlayerPawnBase> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("HideCategories"), TEXT("Navigation"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("PlayerPawnBase.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("PlayerPawnBase.h"));
				MetaData->SetValue(NewProp_SnakeActorClass, TEXT("Category"), TEXT("PlayerPawnBase"));
				MetaData->SetValue(NewProp_SnakeActorClass, TEXT("ModuleRelativePath"), TEXT("PlayerPawnBase.h"));
				MetaData->SetValue(NewProp_SnakeActor, TEXT("Category"), TEXT("PlayerPawnBase"));
				MetaData->SetValue(NewProp_SnakeActor, TEXT("ModuleRelativePath"), TEXT("PlayerPawnBase.h"));
				MetaData->SetValue(NewProp_PawnComponent, TEXT("Category"), TEXT("PlayerPawnBase"));
				MetaData->SetValue(NewProp_PawnComponent, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_PawnComponent, TEXT("ModuleRelativePath"), TEXT("PlayerPawnBase.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(APlayerPawnBase, 3867556591);
	static FCompiledInDefer Z_CompiledInDefer_UClass_APlayerPawnBase(Z_Construct_UClass_APlayerPawnBase, &APlayerPawnBase::StaticClass, TEXT("/Script/SnakeGame4"), TEXT("APlayerPawnBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APlayerPawnBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
PRAGMA_ENABLE_OPTIMIZATION
