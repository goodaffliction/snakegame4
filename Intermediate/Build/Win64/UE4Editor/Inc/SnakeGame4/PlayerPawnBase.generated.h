// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEGAME4_PlayerPawnBase_generated_h
#error "PlayerPawnBase.generated.h already included, missing '#pragma once' in PlayerPawnBase.h"
#endif
#define SNAKEGAME4_PlayerPawnBase_generated_h

#define SnakeGame4_Source_SnakeGame4_PlayerPawnBase_h_15_RPC_WRAPPERS
#define SnakeGame4_Source_SnakeGame4_PlayerPawnBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define SnakeGame4_Source_SnakeGame4_PlayerPawnBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPlayerPawnBase(); \
	friend SNAKEGAME4_API class UClass* Z_Construct_UClass_APlayerPawnBase(); \
public: \
	DECLARE_CLASS(APlayerPawnBase, APawn, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/SnakeGame4"), NO_API) \
	DECLARE_SERIALIZER(APlayerPawnBase) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define SnakeGame4_Source_SnakeGame4_PlayerPawnBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAPlayerPawnBase(); \
	friend SNAKEGAME4_API class UClass* Z_Construct_UClass_APlayerPawnBase(); \
public: \
	DECLARE_CLASS(APlayerPawnBase, APawn, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/SnakeGame4"), NO_API) \
	DECLARE_SERIALIZER(APlayerPawnBase) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define SnakeGame4_Source_SnakeGame4_PlayerPawnBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APlayerPawnBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APlayerPawnBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlayerPawnBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlayerPawnBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlayerPawnBase(APlayerPawnBase&&); \
	NO_API APlayerPawnBase(const APlayerPawnBase&); \
public:


#define SnakeGame4_Source_SnakeGame4_PlayerPawnBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlayerPawnBase(APlayerPawnBase&&); \
	NO_API APlayerPawnBase(const APlayerPawnBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlayerPawnBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlayerPawnBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APlayerPawnBase)


#define SnakeGame4_Source_SnakeGame4_PlayerPawnBase_h_15_PRIVATE_PROPERTY_OFFSET
#define SnakeGame4_Source_SnakeGame4_PlayerPawnBase_h_12_PROLOG
#define SnakeGame4_Source_SnakeGame4_PlayerPawnBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame4_Source_SnakeGame4_PlayerPawnBase_h_15_PRIVATE_PROPERTY_OFFSET \
	SnakeGame4_Source_SnakeGame4_PlayerPawnBase_h_15_RPC_WRAPPERS \
	SnakeGame4_Source_SnakeGame4_PlayerPawnBase_h_15_INCLASS \
	SnakeGame4_Source_SnakeGame4_PlayerPawnBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeGame4_Source_SnakeGame4_PlayerPawnBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame4_Source_SnakeGame4_PlayerPawnBase_h_15_PRIVATE_PROPERTY_OFFSET \
	SnakeGame4_Source_SnakeGame4_PlayerPawnBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	SnakeGame4_Source_SnakeGame4_PlayerPawnBase_h_15_INCLASS_NO_PURE_DECLS \
	SnakeGame4_Source_SnakeGame4_PlayerPawnBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SnakeGame4_Source_SnakeGame4_PlayerPawnBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
